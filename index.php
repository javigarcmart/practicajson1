<?php

if (isset($_POST["nombre"])) {
    echo "<pre>";
    print_r($_POST);
    echo "</pre>";
}

?>

<!doctype html>
<html lang="es" xmlns="http://www.w3.org/1999/html">
<head>

    <style>
        .select-editable { position:relative; background-color:white; border:solid grey 1px;  width:120px; height:18px; }
        .select-editable select { position:absolute; top:0px; left:0px; font-size:14px; border:none; width:120px; margin:0; }
        .select-editable input { position:absolute; top:0px; left:0px; width:100px; padding:1px; font-size:12px; border:none; }
        .select-editable select:focus, .select-editable input:focus { outline:none; }
    </style>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#inputAlumno').on('keyup',function () {
                $.post("datos.php",
                    {
                        letra: $('#inputAlumno').val.();
                        //name: "Donald Duck",
                        //city: "Duckburg"
                    },
                    function(data, status){
                        //alert("Data (Lo que recibe): " + data + "\nStatus (Código de respuesta): " + status);
                        //---- AQUÍ HAY QUE PONER APPEND SI QUEREMOS QUE SE PUEDA REALIZAR VARIAS VECES Y HTML SOLO UNA ----
                        $("#alumno option").remove();


                        for (var i=0; i<data.length; i++){
                            $("#respuesta").append(data[i].nombre+"</br>")
                            $("#alumno").append('<option>'+data[i].nombre+'</option>');
                        }

                    },'json');
            });

            $("#ajax").click(function(){
                $.post("datos.php",
                    {
                        name: "Donald Duck",
                        city: "Duckburg"
                    },
                    function(data, status){
                        //alert("Data (Lo que recibe): " + data + "\nStatus (Código de respuesta): " + status);
                        //---- AQUÍ HAY QUE PONER APPEND SI QUEREMOS QUE SE PUEDA REALIZAR VARIAS VECES Y HTML SOLO UNA ----
                            $("#alumno option").remove();


                        for (var i=0; i<data.length; i++){
                            $("#respuesta").append(data[i].nombre+"</br>")
                            $("#alumno").append('<option>'+data[i].nombre+'</option>');
                        }

                    },'json');
            });
        })

    </script>
</head>
<body>

<form action="index.php" method="post">
    <input type="text" name="nombre" />

    <select name="sexo">
        <option value="hombre">Hombre</option>
        <option value="mujer">Mujer</option>
    </select>

<h2>#InputAlumno</h2>
    <div class="select-editable">
        <select id="alumno" name="alumno"></select>
        <input type="text" name="format" id="inputAlumno" value=""/>
    </div>

    <input type="submit" value="Enviar" />

</form>
<button id="ajax">Ajax</button>
<div id="respuesta"></div>

</body>
</html>